* Files in the bases folder are used as databases. I think it would be correct to divide all the information into different bases,
  in order not to mix and overlap data. Navigation through the files will also be much easier, since it would be more clear where and what information could be found. 
  Our databases will be in the bases folder as json files. For the first time, the base in this form will be enough, because there won`t be a lot of information. Depending on the size of the application, you can choose a more convenient storage option.
  For example, I would use MongoDB as an alternative.
    * * As the application will grow and, possibly, functionality will be added, such as controlling the number of people who are on the beach, etc. It would be very convenient, if each user will be able to see existence of free places and will know what to do and where to go. In the same way, you can display which tracks are occupied and which are not. Therefore, there may be such databases:
      - - difficulty levels of the tracks (trackLvls.json). It will have tracks for people who are currently on it and, for example, wakeboards which these people use.
      - - beach levels (beachLvls.json). The number of people will be displayed for each level. For example, when a person left, delete him from the database, and change the total number of people at this level.
      - - staff (staff.json)
      - - services (services.json)
      - - users (users.json) It will store authorization and personal data of users (including data on payment, achievements, etc.), as well as data of the administrator, trainer and bartender.
      - - bar information (bar.json) The assortment, prices, and others will be stored here. From this base data will be taken to compose the menu
      - - information on equipment (equipment.json)
      - - information on training, including subscriptions, etc. (training.json)
      - - payments (payments.json) This base will contain of basic user information and his money account status. This base will save history of all payments to be able to generate invoice. This base will be linked with users base.